import unittest
import random
from arthmetic import BubbleSort,InsertionSort

class ProductTestCase(unittest.TestCase):
    sort_list = list(range(-10, 100))
    unsort_list = sort_list[:]
    random.shuffle(unsort_list)
    def __test_model(self,func):
        func_result = func(self.unsort_list)
        print(func_result)
        self.assertTrue(self.sort_list == func_result, '%s Test Faild'%func.__name__)

    def testBubbleSort(self):
        self.__test_model(BubbleSort)

    def InsertionSort(self):
        self.__test_model(InsertionSort)


if __name__ == '__main__': unittest.main()