def BubbleSort(unsort_list):
    for i in range(len(unsort_list)):                          #循环列表长度次后，完成排序
        loop_index = 0                                          #循环计次,每次循环以0位置元素开始比较
        end_index = len(unsort_list)                            #列表总长度
        for value in unsort_list[1:end_index]:                 #从1位置元素开始取出，与前位置元素比较
            if unsort_list[loop_index] > value:                #如果前位置元素大于当前元素，则互换位置
                unsort_list[loop_index], unsort_list[loop_index+1] = unsort_list[loop_index+1],unsort_list[loop_index]
            loop_index += 1                                    #每次循环起始为加1，即遍历列表，每每相邻进行比较
            end_index -=1                                      #循环一次后，列表末尾元素为最大元素
    sort_list = unsort_list
    return sort_list

def InsertionSort(unsort_list):
    sort_list = [unsort_list.pop(0)]                            #弹出第一个元素，生成排序列表
    for i in range(len(unsort_list)):                           #循环列表长度次后，完成排序
        pop_value = unsort_list.pop(0)                          #弹出当前第一个元素
        loop_index = len(sort_list)                             #新元素插入的位置
        for value in sort_list[::-1]:                          #反转已排序的列表，使最大值在第一位
            if pop_value > value:
                sort_list.insert(loop_index,pop_value)
                break
            else:
                if loop_index == 1:                             #如果已排序列表中没有比其小的元素，则插入排序列表第一位
                    sort_list.insert(0,pop_value)
            loop_index -=1                                      #插入索引每次减1
    return sort_list
